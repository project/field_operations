
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation]
 * Maintainers

INTRODUCTION
------------

 * Field Operations module is used to do all operations of field like edit, delete, 
   Storage settings in one page.
 * This module will list all used fields across site and able to do all operations.

REQUIREMENTS
------------
This module requires the following modules:
 * Field(field)
 * FieldUI(field_ui)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

MAINTAINER
-----------
Current maintainers:
 * A AjayKumar Reddy (ajay_reddy) - https://www.drupal.org/u/ajay_reddy
